-- Create Tables
CREATE TABLE warehouses (
    warehouse_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(100) NOT NULL
);

CREATE TABLE carriers (
    carrier_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    contact_person VARCHAR(50),
    contact_number VARCHAR(15)
);

CREATE TABLE shipments (
    shipment_id SERIAL PRIMARY KEY,
    tracking_number VARCHAR(20) UNIQUE NOT NULL,
    weight DECIMAL(10, 2) NOT NULL,
    status VARCHAR(20) DEFAULT 'Pending',
    warehouse_id INT REFERENCES warehouses(warehouse_id),
    carrier_id INT REFERENCES carriers(carrier_id)
);

-- Seed Data
INSERT INTO warehouses (name, location) VALUES
    ('Warehouse A', 'Location A'),
    ('Warehouse B', 'Location B');

INSERT INTO carriers (name, contact_person, contact_number) VALUES
    ('Carrier X', 'John Doe', '123-456-7890'),
    ('Carrier Y', 'Jane Smith', '987-654-3210');

INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id) VALUES
    ('ABC123', 150.5, 1, 1),
    ('XYZ789', 200.0, 2, 2);

-- Task 1a
CREATE VIEW warehouse_shipments AS
SELECT s.tracking_number, s.weight, s.status, w.name AS warehouse_name
FROM shipments s
INNER JOIN warehouses w on s.warehouse_id = w.warehouse_id;

-- Task 1b
CREATE VIEW carrier_shipments AS
SELECT s.tracking_number, s.weight, s.status, c.name AS carrier_name
FROM shipments s
INNER JOIN carriers c on s.carrier_id = c.carrier_id;

-- Task 2a
WITH pending_shipments AS (
	SELECT s.tracking_number, s.weight, w.location
	FROM shipments s
	INNER JOIN warehouses w on s.warehouse_id = w.warehouse_id
	WHERE s.status = 'Pending'
)
SELECT * FROM pending_shipments;

-- Task 2b
WITH heavy_shipments AS (
	SELECT s.tracking_number, s.weight, c.name AS carrier_name
	FROM shipments s
	INNER JOIN carriers c ON s.carrier_id = c.carrier_id
	WHERE s.weight > 200
)
SELECT * FROM heavy_shipments;

-- Task 3a
BEGIN;
UPDATE shipments 
SET status = 'Shipped', weight = weight + 10
WHERE tracking_number = 'ABC123';
COMMIT;

-- Task 3b
BEGIN;
INSERT INTO shipments (tracking_number, weight, warehouse_id, carrier_id)
VALUES 
('LMN456', 
 180.75, 
 (SELECT warehouse_id FROM warehouses WHERE name = 'Warehouse B'), 
 (SELECT carrier_id FROM carriers WHERE name = 'Carrier Y')
);
COMMIT;

-- Task 4
-- Creating an index on tracking number since it is the column being searched on in the example query
CREATE INDEX index_tracking_number ON shipments(tracking_number);

